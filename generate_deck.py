from PIL import Image, ImageDraw, ImageFont, ImageColor
from textwrap import fill
from typing import List
import copy

title_font = 'julee'
description_font = 'Roboto'


def get_columns_and_rows_number(array: List[str]):
    array_length = len(array)
    if array_length >= 70:
        print('Error, too much cards for a single card sheet')

    if array_length >= 10:
        columns = int(str(array_length)[1])
        rows = int(str(array_length)[0])
        return columns, rows
    else:
        columns = int(str(array_length)[0])
        return columns, 0


def generate_back(text, card_height_pixels, color_theme):
    back_graphic_ref = Image.open(f'assets/card/{color_theme}_back.png', 'r')
    back_graphic = copy.deepcopy(back_graphic_ref)
    image_title_font = ImageFont.truetype(f'fonts/{title_font}.ttf', 75)
    draw = ImageDraw.Draw(back_graphic)
    draw.text((90, card_height_pixels/3),
              text.upper(), font=image_title_font, fill=(0, 0, 0))
    return back_graphic


def generate_deck_sheet(card_width_pixels, card_height_pixels, max_columns, max_rows, all_cards_data_array, color_theme):
    deck_sheet = Image.new(mode="RGBA", size=(card_width_pixels * max_columns,
                                              card_height_pixels * max_rows))
    columns, rows = get_columns_and_rows_number(all_cards_data_array)
    card_template_image_ref = Image.open(f'assets/card/{color_theme}.png', 'r')
    for row in range(0, rows + 1):  # It doesn't run for (rows), stops at (rows-1)
        # It doesn't run for (columns), stops at (columns-1)
        for column in range(0, max_columns):
            if row == rows and column == columns:
                break
            else:
                card_data_array = all_cards_data_array[column*1+row*10]
                card_template_image = copy.deepcopy(card_template_image_ref)
                card = get_card(card_data_array, card_template_image)
                deck_sheet.paste(card,
                                 (column*card_width_pixels, row*card_height_pixels))

    return deck_sheet


def generate(card_width_pixels, card_height_pixels, max_columns, max_rows, all_cards_data_array, color_theme, filename, card_type, mode):
    deck_sheet = generate_deck_sheet(
        card_width_pixels, card_height_pixels, max_columns, max_rows, all_cards_data_array, color_theme)
    back_image = generate_back(card_type, card_height_pixels, color_theme)

    if mode == 'save':
        deck_sheet.save(f"results/{filename}.png")
        back_image.save(f"results/{filename}_back.png")
    else:
        deck_sheet.show()
        back_image.show()


title = "title"
description = "description"
left_icon = "left_icon"
right_icon = "right_icon"
left_icon_text = "left_icon_text"


def get_card(card_data_array, card_template_image):
    card_data = {
        title: card_data_array[0],
        description: card_data_array[1],
        left_icon: card_data_array[2],
        right_icon: card_data_array[3],
        left_icon_text: card_data_array[4],
    }
    return generate_card(card_data, card_template_image)


def generate_card(card_data, card_template_image):
    image_title_font = ImageFont.truetype(f'fonts/{title_font}.ttf', 30)
    image_description_font = ImageFont.truetype(
        f'fonts/{description_font}.ttf', 15)

    if card_data[right_icon] != "":
        right_icon_image = Image.open(
            f'assets/icons/{card_data[right_icon]}.png', 'r').convert("RGBA")
        right_icon_image.thumbnail((50, 50))
        card_template_image.paste(
            right_icon_image, (332, 505), mask=right_icon_image)

    if card_data[left_icon] != "":
        left_icon_image = Image.open(
            f'assets/icons/{card_data[left_icon]}.png', 'r').convert("RGBA")
        left_icon_image.thumbnail((50, 50))
        card_template_image.paste(
            left_icon_image, (29, 505), mask=left_icon_image)

    draw = ImageDraw.Draw(card_template_image)
    draw.text((47, 512), card_data[left_icon_text],
              font=image_title_font, fill=(0, 0, 0))

    title_text = fill(card_data[title], width=20)
    draw.text((70,  300),
              title_text, font=image_title_font, fill=(0, 0, 0))
    description_text = fill(card_data[description], width=35)
    draw.text((85, 360),
              description_text, font=image_description_font, fill=(0, 0, 0))

    if card_data[left_icon_text] != "":
        draw.text((47, 512), card_data[left_icon_text],
                  font=image_title_font, fill=(0, 0, 0))

    return card_template_image
