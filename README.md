# Setup

Create a `results` and `decks` directory and put your `.txt` file/files inside the latter. Content of each of those files should follow the pattern:

- First line of each file should be the deck data, separated with semicolon:

  ```
  card_width_pixels;card_height_pixels;color_theme;card_back_text
  ```

  Currently, `color_theme` can be one of the selected values: `red`, `blue`, `green`, `grey`.

- Second line and onwards should be the cards data, each line representing a single card, data separated with semicolon:
  ```
  title;description;left_icon_filename;right_icon_filename;left_icon_text;
  ```
  Each data entry for the card is optional, e.g. if you want to leave 1 blank card, just leave `;;;;;` line. It is important to keep the proper amount of semicolons (6) in any case! Icons filenames can be one of the following: `bag`, `bow`, `cave`, `diamond`, `forest`, `heart`, `rock`, `shield`, `swamp`, `sword`. All of the icons are subject to the MIT license - license holders list will be provided soon.

Example of such a file content:

```
410;586;red;Test Back
Test Card 1;Description for the test card 1.;diamond;bow;;
Test Card 2;Description for the test card 2.;bag;bag;;
Test Card 3;;heart;sword;1;
Test Card 4;Description for the test card 4.;;;
```

For ease of use and testing, one can copy and paste `deck_example.txt` file from the root of this project into the newly created `decks` directory.

# Running

To execute the script, run `main.py` with no parameters. Make sure the `Setup` steps are fulfilled before that happens.

The results of the run will be saved in the `results` directory. Each TXT file from the `decks` directory will correspond to 2 PNG files in the the results directory, one with the same name as the original file and one with `_back` suffix. First one contains the template of the deck, while the second only a single card back.
