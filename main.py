from generate_deck import generate
from os import walk

directory = 'decks'
filenames = next(walk(directory), (None, None, []))[2]

mode = 'save'  # save - to save to files, show - to open in a photo viewer

for filename in filenames:
    with open(f'{directory}/{filename}') as file:

        # First file line is a data
        card_width_pixels, card_height_pixels, color_theme, card_type = next(
            file).strip().split(';')

        card_width_pixels = int(card_width_pixels)
        card_height_pixels = int(card_height_pixels)

        # From the 2nd line, everything else is cards data
        all_cards_data_array = [line.split(';') for line in file]
        generate(
            card_width_pixels, card_height_pixels, 10, 7, all_cards_data_array, color_theme, filename[:-4], card_type, mode)
